package com.example.examen_mobil_1

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.os.CountDownTimer
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.navArgs

import com.evernote.android.state.State
import com.evernote.android.state.StateSaver


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Operation.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Operation.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Operation : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    internal lateinit var round: TextView
    var roundTotal = 0
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var time: TextView
    internal lateinit var primerNumero: TextView
    internal lateinit var segundoNumero: TextView
    internal lateinit var simbolo: TextView
    internal lateinit var respuesta: TextView
    internal lateinit var button_send: Button
    @State
    var score = 0
    @State
    var timeLeft = 10

    @State
    var gameStarted = true

    private val args: OperationArgs  by navArgs()


   // private val args: OperationArgs  by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private lateinit var countDownTimer: CountDownTimer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_operation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val r = (1..30).shuffled().first()
        val r2 = (1..30).shuffled().first()
        val operador = args.opc
        round = view.findViewById(R.id.round)
        gameScoreTextView = view.findViewById(R.id.score)
        time = view.findViewById(R.id.time)
        primerNumero = view.findViewById(R.id.primerNumero)
        segundoNumero = view.findViewById(R.id.SegundoNumero)
        simbolo = view.findViewById(R.id.simbolo)
        simbolo.text = operador
        primerNumero.text = ""+r
        segundoNumero.text = ""+r2
        time.text = getString(R.string.time_left_d, timeLeft)
        gameScoreTextView.text = getString(R.string.score_d, score)

        button_send = view.findViewById(R.id.button_send)
        button_send.setOnClickListener{incrementScore(primerNumero.text.toString(),segundoNumero.text.toString(),operador)}




    }



    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000

    private fun resetGame(){
        score = 0
        gameScoreTextView.text = getString(R.string.score_d, score)

        timeLeft = 10
        time.text = getString(R.string.time_left_d, timeLeft)

        countDownTimer = object:CountDownTimer(initialCountDown, countDownInterval){

            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
                time.text = getString(R.string.time_left_d, timeLeft)
            }
        }
        gameStarted = false
    }

    private fun incrementScore(primerNumeroF:String, segundoNumeroF:String, operador:String){


        if(gameStarted){
            countDownTimer = object:CountDownTimer(initialCountDown, countDownInterval){

                override fun onFinish() {
                    endGame()
                }

                override fun onTick(millisUntilFinished: Long) {
                    timeLeft = millisUntilFinished.toInt()/1000
                    time.text = getString(R.string.time_left_d, timeLeft)
                }
            }
            countDownTimer.start()
            gameStarted = false
        }

    var resultadoTemp:Float = 0F

    when(operador){
      "+" -> resultadoTemp = primerNumeroF.toFloat() + segundoNumeroF.toFloat()
      "-" -> resultadoTemp = primerNumeroF.toFloat() - segundoNumeroF.toFloat()
      "*" -> resultadoTemp = primerNumeroF.toFloat() * segundoNumeroF.toFloat()
      "/" -> resultadoTemp = primerNumeroF.toFloat() / segundoNumeroF.toFloat()
     }
        respuesta = view!!.findViewById(R.id.res)

    if((resultadoTemp)==respuesta.text.toString().toFloat()){
        score ++
        gameScoreTextView.text = getString(R.string.score_d, score)
    }

        val r = (1..30).shuffled().first()
        val r2 = (1..30).shuffled().first()
        primerNumero.text = ""+r
        segundoNumero.text = ""+r2

    }



    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score_d, score)

        countDownTimer = object:CountDownTimer(10*1000L, 1000){

            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
                time.text = getString(R.string.time_left_d, timeLeft)
            }
        }
        countDownTimer.start()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun startGame(){
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame(){
        Toast.makeText(
            activity,
            getString(R.string.end_game,score),
            Toast.LENGTH_LONG).show()
            roundTotal++
            round.text = ""+roundTotal
            gameStarted= true
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Operation.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Operation().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
